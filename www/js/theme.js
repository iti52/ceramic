/**
 * Created by Squirrel on 14.08.15.
 */

jQuery(document).ready(function(){
    jQuery('.select-item-body').hide();
});

jQuery('.select-item .select-item-head').click(function() {
    jQuery(this).find('span.select-state').toggleClass('select-point-open');
    jQuery(this).parent().find('.select-item-body').toggle(300);
});


jQuery(document).ready(function(){
    jQuery('.select-form-sm').hide();
});

jQuery('.select-block-filter-link').click(function() {
    jQuery('.select-form-sm').toggle(300);
});


jQuery('#flat-slider').slider({
    orientation: 'horizontal',
    min:         0,
    max:         1000,
    values:      [0,1000],
    range:       true,
        stop: function(event, ui) {
            jQuery("input#minCost").val(jQuery("#flat-slider").slider("values",0));
            jQuery("input#maxCost").val(jQuery("#flat-slider").slider("values",1));
        },
        slide: function(event, ui){
            jQuery("input#minCost").val(jQuery("#flat-slider").slider("values",0));
            jQuery("input#maxCost").val(jQuery("#flat-slider").slider("values",1));
        }
});

jQuery("input#minCost").change(function(){
    var value1=jQuery("input#minCost").val();
    var value2=jQuery("input#maxCost").val();

    if(parseInt(value1) > parseInt(value2)){
        value1 = value2;
        jQuery("input#minCost").val(value1);
    }
    jQuery("#flat-slider").slider("values",0,value1);
});


jQuery("input#maxCost").change(function(){
    var value1=jQuery("input#minCost").val();
    var value2=jQuery("input#maxCost").val();

    if (value2 > 1000) { value2 = 1000; jQuery("input#maxCost").val(1000)}

    if(parseInt(value1) > parseInt(value2)){
        value2 = value1;
        jQuery("input#maxCost").val(value2);
    }
    jQuery("#flat-slider").slider("values",1,value2);
});



